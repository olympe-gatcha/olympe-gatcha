module.exports = {
  apps : [{
    name: 'gatcha',
    script: 'npm',
    args: 'start',
    exec_interpreter: 'node@16.6.2',
  }, {
    script: './service-worker/',
    watch: ['./service-worker']
  }],

  deploy : {
    production : {
      user : process.env.SSH_USERNAME,
      host : process.env.SSH_HOSTMACHINE,
      port : process.env.SSH_PORT,
      key  : 'deploy.key',
      ref  : 'origin/master',
      repo : 'https://gitlab.com/olympe-gatcha/olympe-gatcha.git',
      path : process.env.DESTINATION_PATH,
      'pre-deploy-local': '',
      'post-deploy' : 'git submodule update --init && nvm use && npm install && nvm use default && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
