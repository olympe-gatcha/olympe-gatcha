import './Tool'

export const LinesTable = new Map<number, TLine>();

export class TLine {
    id: number;
    name: string;
    formatString: string;

    dataRange?: [number, number][];

    constructor(id: number, name: string, formatString: string, dataRange?: [number, number][]) {
        this.id = id;
        this.name = name;
        this.formatString = formatString;
        this.dataRange = dataRange;

        if (LinesTable.has(id)) {
            throw `nope line ${id} ${name} is already assigned to ${LinesTable.get(id).name}`;
        }
        LinesTable.set(id, this);
    }

    effect?: Map<string, any>;

    string() : string {
        return this.formatString.format(...(this.dataRange ? this.dataRange.map(
            (v: [number, number])=>`[ ](${v[0]} - ${v[1]})`
        ) : undefined));
    }

    rollLine(...data: number[]) : Line {
        const ret = new Line(this.id, this.dataRange?.map((v: [number, number], i: number)=>{
            return data[i] || Math.floor(Math.random() * (v[1] - v[0] + 1)) + v[0];
        }));

        return ret;
    }
}


export class Line {
    idLine: number;
    data: number[];

    constructor(idLine: number, data : number[]) {
        this.idLine = idLine;
        this.data = data;
    }

    string() : string {
        const line = LinesTable.get(this.idLine)
        return line.formatString.format(...this.data?.map((v, i)=>`[${v}](${line.dataRange[i][0]} - ${line.dataRange[i][1]})`));
    }

    name() : string {
        return LinesTable.get(this.idLine).name;
    }
}

import './LinesTable'