export function weightedPick(chances: number[], total: number): number {
    const val = Math.floor(Math.random() * total);
    var cur = 0;
    for (var i = 0; i < chances.length; i++) {
        cur += chances[i];
        if (val <= cur) {
            return i;
        }
    }
    return undefined;
}

declare global {
    interface String {
        format(...replacements: string[]): string;
    }
}

String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
            ? args[number]
            : match
            ;
    });
};