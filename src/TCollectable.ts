import { User } from "./entity/User";

export class TCollectable {
    string() {
        throw `Don't use TCollectable directly`
    }

    async addToUser(user: User, quantity:number = 1) : Promise<any[]>{
        throw `Don't use TCollectable directly`
    }
}