import { CommandInteraction, Interaction } from "discord.js";
import { Command, CommandData } from "./Command";
import { User } from "./entity/User";

export const has_user = async (i: CommandData) : Promise<CommandData> => {
    const user = await User.findOne({discordId: i.inter.member.user.id}, {relations: ['selectedTeam']});

    if (user) {
        user.name = i.inter.user.username;
        i.user = user;
        return i;
    }
    throw "You don't have an acount yet say 'gatcha' to start";
}

export const has_selected_team = async (i:CommandData) : Promise<CommandData> => {
    if (i.user.selectedTeam) {
        return i;
    }
    throw 'You have no team selected (select with /team select teamName)';
}