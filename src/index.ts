import "reflect-metadata";
import { createConnection } from "typeorm";
import { ApplicationCommandData, Guild, Interaction } from "discord.js";
import { CommandList } from "./CommandList";
import { Command } from "./Command";
import { client } from "./discord_client";
import { gatcha } from "./commands/gatcha";
import { importInvokeFromCSV } from './InvokeTable';
import { generateDefaultBaner } from "./BannerList";

require('dotenv').config();

if (!process.env.DISCORD_CHANNEL_ID || !process.env.DISCORD_TOKEN) {
    console.log(process.env.DISCORD_TOKEN);
    console.log('Missing env (create a .env file with the DISCORD_CHANNEL_ID DISCORD_TOKEN info');
    process.exit(1);
}

client.once('ready', ()=>{
    console.log('Registering commands');
    const cmds : ApplicationCommandData[] = [];
    CommandList.forEach((c: Command)=>{
        cmds.push(c.schema());
    });
    client.guilds.cache.forEach(async (guild: Guild) => {
        await guild.commands.set(cmds);
    });
    console.log('Discord Ready');
});

client.on('interactionCreate', (interaction: Interaction) => {
    if (!interaction.isCommand()) {
        return;
    }
    var cmd = CommandList.get(interaction.commandName)
    if (cmd) {
        if (cmd.name == 'channel_id') {
            cmd.execute(interaction);
        } else if (interaction.channelId != process.env.DISCORD_CHANNEL_ID) {
            interaction.reply({content : 'This is the wrong channel for this command (you probably need to be in the gatcha channel)', ephemeral: true});
        } else {
            cmd.execute(interaction);
        }
    }
});

createConnection().then(async connection => {
    await initServer();
    console.log('db Ready');
    await client.login(process.env.DISCORD_TOKEN);
}).catch(error => console.log(error));

async function initServer() {
    await importInvokeFromCSV();
    generateDefaultBaner();
    try {
        await gatcha('server', 'server', []);
    } catch(e) {
        console.log(e);
    }
}