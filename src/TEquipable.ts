import { Board } from "./Board";
import { Color } from "./Color";
import { CharInstance } from "./entity/CharInstance";
import { Tier } from "./Tier";
import { Line } from "./TLine";
import { EquipablesTable } from './EquipablesTable';
import { Equipable } from "./entity/Equipable";
import { User } from "./entity/User";
import { LinesTable } from "./TLine";
import { TCollectable } from './TCollectable';
import { weightedPick } from './Tool';

export class TEquipable extends TCollectable {
    id: number;

    name: string;

    color: Color;

    tierMin: Tier;
    tierMax: Tier;

    implicit: Line;

    rollable_prefixes: Map<Tier, Array<[number, number]>>
    rollable_sufixes: Map<Tier, Array<[number, number]>>

    active?:(b: Board, source: CharInstance, target: CharInstance[], reply: string[])=>void;
    findTarget?:(b: Board, source: CharInstance)=>CharInstance[];

    constructor(
        id: number,
        name: string,
        color: Color,
        tierMin: Tier,
        implicit: Line,
        rollable_prefixes: Map<Tier, Array<[number, number]>>,
        rollable_sufixes: Map<Tier, Array<[number, number]>>,
        active?: (b: Board, source: CharInstance, target: CharInstance[], reply: string[])=>void,
        findTarget?:(b: Board, source: CharInstance)=>CharInstance[]) 
    {
        super();
        this.id = id;
        this.name = name;
        this.color = color;
        this.tierMin = tierMin;
        this.rollable_prefixes = rollable_prefixes;
        this.rollable_sufixes = rollable_sufixes;
        this.implicit = implicit;
        this.active = active;
        this.findTarget = findTarget;
        
        //Calculate tier max from available line
        for (let i = Tier.C; i > tierMin; i--) {
            if (this.rollable_prefixes.has(i) || this.rollable_sufixes.has(i)) {
                this.tierMax = i;
            }
        }
        if (!this.tierMax) {
            this.tierMax = this.tierMin;
        }

        if (EquipablesTable.has(id)) {
            throw `nope equipable ${id} ${name} is already assigned to ${EquipablesTable.get(id).name}`;
        }
        EquipablesTable.set(id, this);
    }

    rollEquipable(user: User, tier?: Tier, base?: Equipable) : Equipable {
        let ret = base ? base : new Equipable();

        if (!tier) {
            tier = this.tierMin;
        }

        ret.user = user;
        ret.eqId = this.id;
        ret.tier = tier;
        ret.implicit = this.implicit;

        ret.prefix = this.rollPrefix(tier);
        ret.suffix = this.rollSuffix(tier);

        return ret;
    }

    rollPrefix(tier: Tier) {
        let rp = [];

        this.rollable_prefixes.forEach((v, t) => {
            if (t <= tier) {
                rp.push(...v);
            }
        });

        return rp.length == 0 ? LinesTable.get(0).rollLine() : LinesTable.get(rp[weightedPick(rp.map(v=>v[1]), rp.reduce((prev, cur)=> prev + cur[1],0))][0]).rollLine();
    }

    rollSuffix(tier: Tier) {
        let rs = [];

        this.rollable_sufixes.forEach((v, t) => {
            if (t <= tier) {
                rs.push(...v);
            }
        });
        return rs.length == 0 ? LinesTable.get(0).rollLine() : LinesTable.get(rs[weightedPick(rs.map(v=>v[1]), rs.reduce((prev, cur)=> prev + cur[1],0))][0]).rollLine();
    }

    string() {
        //TODO
        return `${this.name} (${this.tierMin} - ${this.tierMax})`;
    }

    async addToUser(user: User, quantity:number = 1) {
        const ret = [];
        for (; quantity > 0; quantity--) {
            const i = this.rollEquipable(user)
            ret.push(i);
            await i.save();
        }
        return ret;
    }
}