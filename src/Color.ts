export enum Color {
    White = 'W',
    Red = 'R',
    Green = 'G',
    Blue = 'B',
};

export function colorFromString(s: string) : Color {
    switch (s) {
        case 'W': {
            return Color.White;
        }
        case 'R': {
            return Color.Red;
        }
        case 'G': {
            return Color.Green;
        }
        case 'B': {
            return Color.Blue;
        }
        default: {
            throw s + ' is not a color';
        }
    }
}