import { Stats } from "./Stats";
import { Color } from './Color';
import { Tier, TierString } from "./Tier";
import { TCollectable } from './TCollectable';
import { User } from "./entity/User";
import { Invoke } from './entity/Invoke';

export class TInvoke extends TCollectable {
    id: number;
    name: string;
    slots: Array<Color>;
    stats: Stats;
    tier: Tier;
    evo? : number;

    constructor(
        id: number,
        name: string,
        slots: Array<Color>,
        tier: Tier,
        stats: Stats,
        evo? : number,
        ) {
            super();
            this.id = id;
            this.name = name;
            this.slots = slots;
            this.tier = tier;
            this.stats = stats;
            this.evo = evo;
        }

    string() {
        return `${TierString[this.tier]} ${this.name}[${this.id}]`
    }

    async addToUser(u: User, quantity: number = 1) {
        let invoke = await Invoke.findOne({user: u, invokeId: this.id});
        if (invoke) {
            invoke.count += quantity;
        } else {
            invoke = new Invoke();
            invoke.user = u;
            invoke.invokeId = this.id;
            invoke.count = quantity;
        }

        await invoke.save();
        return [invoke];
    }
}