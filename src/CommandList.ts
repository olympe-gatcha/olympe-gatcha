import { Command } from "./Command";

export const CommandList : Map<string, Command> = new Map();

import './commands';