import { TeamInstance } from './entity/TeamInstance';
import { CharInstance } from './entity/CharInstance';
import { EquipablesTable } from './EquipablesTable';
import { MSG_BRK } from './Command';

const MAX_TURN = 20;

export class Board {
    done : boolean = false;
    turn : number = 1;
    result : TeamInstance = undefined;
    chars : CharInstance[];
    diedThisTurn : number[] = [];

    constructor(team1: TeamInstance, team2: TeamInstance) {
        this.chars = team1.characters.concat(team2.characters);
    }

    fight(reply: string[]) {
        while (!this.done) {
            this.startTurn(reply);
            this.chars.sort((a: CharInstance, b: CharInstance)=>{
                return b.stats.spd - a.stats.spd;
            });

            this.chars.forEach(char=>this.charTurn(char, reply));
            this.endTurn(reply);
        }

        return this.result;
    }

    charTurn(char: CharInstance, reply: string[]) {
        if (char.stats.hp <= 0) {
            return;
        }
        const auto = EquipablesTable.get(0);
        const targets = auto.findTarget(this, char);
        auto.active(this, char, targets, reply);
    }

    startTurn(reply : string[]) {
        reply.push('```md');
    }

    endTurn(reply: string[]) {
        this.chars = this.chars.filter((c : CharInstance) => {return (this.diedThisTurn.indexOf(c.character.id) == -1)});
        if (this.chars.length == 0 || this.turn >= MAX_TURN) {
            this.done = true;
            this.result = undefined;
            return;
        }
        var team = this.chars[0].team.team.id;
        var done = true;
        for (var i = 1; i < this.chars.length; i++) {
            if (this.chars[i].team.team.id != team) {
                done = false;
                break;
            }
        }
        if (done) {
            this.done = true;
            this.result = this.chars[0].team;
        }
        this.turn++;
        reply.push('```');
        reply.push(MSG_BRK);
    }

    deathOfChar(char: CharInstance, reply: string[]) {
        reply.push(`${char.string()} is dead ... Lol`);
        this.diedThisTurn.push(char.character.id);
    }
}