


export interface Stats {
    hp: number;
    hpFlat: number;
    hpPrc: number;

    spd: number;

    atk: number;
    atkFlat: number;
    atkPrc: number;

    mag: number;
    magFlat: number;
    magPrc: number;

    def: number;
    defFlat: number;
    defPrc: number;

    spr: number;
    sprFlat: number;
    sprPrc: number;
    
    critChance: number;

    etRes: number;
    eeRes: number;
    epRes: number;

    stunRes: number;
    sleepRes: number;
    poisonRes: number;
    paraRes: number;
    bleedRes: number;
    burnRes: number;
    freezeRes: number;
}

export function statString(stats : Stats) {
    if (!stats) {
        return 'Wrong Stats';
    }
    var ret : string;
    ret = `
    hp: ${stats.hp}
    
    spd: ${stats.spd}
    atk: ${stats.atk}
    mag: ${stats.mag}
    def: ${stats.def}
    spr: ${stats.spr}`;
    return ret;
}