import { colorFromString } from "./Color";
import { tierFromCsv } from "./Tier";
import { TInvoke } from "./TInvoke";
import { Parser } from 'csv-parse'
import { createReadStream } from "fs";

export const InvokeTable = new Map<number, TInvoke>();

export function invokeString(id : number) {
    const invoke = InvokeTable.get(id);
    if (!invoke) {
        return `${id}`;
    }
    return invoke.string();
}

export function importInvokeFromCSV() : Promise<void> {
    return new Promise((resolve, reject)=> {
        const parser = new Parser({delimiter:','});
        parser.on('readable', function() {
            let record : string[];
            while (record = parser.read()) {
                if (record[0] == 'id') {
                    continue;
                }
                try {
                    const invoke : TInvoke = new TInvoke(
                        +record[0],
                        record[1],
                        record[2].split('-').map(v=>colorFromString(v)),
                        tierFromCsv(record[3]),
                        {
                            hp: +record[5],
                            hpFlat: +record[5],
                            hpPrc: 0,
                            
                            spd: +record[6],

                            atk: +record[7],
                            atkFlat: +record[7],
                            atkPrc: 0,

                            mag: +record[8],
                            magFlat: +record[8],
                            magPrc: 0,

                            def: +record[9],
                            defFlat: +record[9],
                            defPrc: 0,

                            spr: +record[10],
                            sprFlat: +record[10],
                            sprPrc: 0,

                            critChance: +record[11],
                            
                            etRes: +record[12],
                            eeRes: +record[13],
                            epRes: +record[14],
                            
                            stunRes: +record[15],
                            sleepRes: +record[16],
                            poisonRes: +record[17],
                            paraRes: +record[18],
                            bleedRes: +record[19],
                            burnRes: +record[20],
                            freezeRes: +record[21],
                        },
                        (record[4] != '') ? +record[4] : undefined,
                    );
                    if (InvokeTable.get(invoke.id)) {
                        throw `Id ${invoke.id} Already exists`;
                    }
                    InvokeTable.set(invoke.id, invoke);
                } catch (e) {
                    reject(`id ${record[0]} error : ${e ?? undefined}`);
                }
            }
        });

        parser.on('end', function(){
            resolve();
        })

        createReadStream('./data/invoke.csv').pipe(parser);
    })
}