import { CollectableType, elemFromTypeId } from "./entity/Shop";
import { TCollectable } from './TCollectable';


export class Banner {
    chances: {id: number, type: CollectableType, quantity?: number, weight: number}[];
    total: number;
    name: string;
    price: number = 0;

    constructor(name: string, c: {id: number, type: CollectableType, quantity?: number, weight: number}[] = []) {
        this.name = name;
        if (!c) {
            return;
        } 
        this.chances = c;
        this.total = c.reduce((ret : number, cur) : number => {
            return ret + cur.weight;
        }, 0)
    }

    add(c: {id: number, type: CollectableType, quantity?: number, weight: number}) {
        this.chances.push(c);
        this.total += c.weight;
    }

    pull() : {collect: TCollectable, quantity: number} {
        const val = Math.floor(Math.random() * this.total);
        var cur = 0;
        for (var i = 0; i < this.chances.length; i++) {
            cur += this.chances[i].weight;
            if (val <= cur) {
                return {
                    collect: elemFromTypeId(this.chances[i].type, this.chances[i].id),
                    quantity: this.chances[i].quantity || 1
                }
            }
        }
        console.log('Failed pull ! (how)')
        return undefined;
    }
}