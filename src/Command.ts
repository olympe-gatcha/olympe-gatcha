import { ApplicationCommandData, ApplicationCommandOptionData, CommandInteraction, Message, StartThreadOptions, ThreadAutoArchiveDuration, ThreadChannel } from "discord.js";
import { User } from "./entity/User";

export interface CommandData {
    inter: CommandInteraction,
    reply: string[],
    user?: User,
    thread?: ThreadChannel,
}

export async function replyOnThread(d: CommandData, initialReply: string, option: StartThreadOptions) {
    if (d.thread) {
        return;
    }
    const msg = await d.inter.reply({content:initialReply, fetchReply:true});
    d.thread = await (msg as Message).startThread(option);
}

export class Command {
    name: string;
    description: string;
    middleWare: {(d: CommandData) : Promise<CommandData>}[] = [];
    subcommand: Command[] = [];
    option: ApplicationCommandOptionData[] = [];

    async action?(d: CommandData) : Promise<void>;

    constructor(name: string) {
        this.name = name;
    }

    schema() : ApplicationCommandData {
        return {
            name: this.name,
            description: this.description,
            options: this.subcommand.map(v=>v.optionSchema()).concat(this.option),
        }
    }

    optionSchema() : ApplicationCommandOptionData {
        return {
            name: this.name,
            description: this.description,
            type: 'SUB_COMMAND',
            options: this.subcommand.map(v=>v.optionSchema()).concat(this.option),
        }
    }

    async execute(inter: CommandInteraction) {
        return this.executeWithData({inter:inter, reply:[]});
    }

    async executeWithData(d: CommandData, final: boolean = true) : Promise<void> {
        await this.middleWare.reduce((prev, cur)=>prev.then(cur), Promise.resolve(d))
        .then(async d=>{
            if (this.action) {
                await this.action(d);
                return d;
            }
            const sub = d.inter.options.getSubcommand(false);
            for (var i = 0; i < this.subcommand.length; i++) {
                if (this.subcommand[i].name == sub) {
                    await this.subcommand[i].executeWithData(d, false);
                    return d;
                }
            }
            throw `Cmd not found '${this.name}' '${sub}'`;
        })
        .then(d=>{
            if (final) {
                if (d.reply.length === 0 && !d.inter.replied) {
                    return d.inter.reply('Nothing to say');
                }
                return sendAndSplit(d);
            }
        })
        .catch((reason)=>{
            console.log('throw', reason);
            if (final && !d.inter.replied) {
                d.inter.reply({content:reason, ephemeral: true});
            }
            else
            throw reason;
        })
    }
}

export const MSG_BRK='msgbrk';

async function sendAndSplit(d:CommandData) {
    //Will fail if a line is longer than 2000 char (fuck you)
    
    var lineStart = 0;
    var curlen = 0;
    var i: number;

    for (i = 0; i < d.reply.length; i++) {
        if (d.reply[i]==MSG_BRK || curlen + d.reply[i].length >= 1500) {
            const msg = d.reply.slice(lineStart, i).join('\n');
            curlen = 0;
            lineStart = (d.reply[i]==MSG_BRK) ? i + 1 : i;
            await sendMessage(d, msg);
        }
        curlen += d.reply[i].length;
    }
    const msg = d.reply.slice(lineStart, i).join('\n');
    if (msg) {
        await sendMessage(d, msg);
    }
}

async function sendMessage(d: CommandData, msg:string) {
    if (d.thread) {
        await d.thread.send(msg);
    } else {
        if (d.inter.replied) {
            await d.inter.followUp(msg);
        } else {
            await d.inter.reply(msg);
        }
    }
}