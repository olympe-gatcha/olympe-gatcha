

export enum Tier {
    K = 0,
    N,
    H,
    MG,
    G,
    T,
    C,
    KO = 200,
}

export const TierString : string[] = [
    'King',
    'Nymph',
    'Hero',
    'Minor God',
    'God',
    'Titan',
    'Chaos',
]

export function tierFromCsv(s:string) : Tier {
    switch (s) {
        case 'K': {
            return Tier.K;
        }
        case 'N': {
            return Tier.N;
        }
        case 'H': {
            return Tier.H;
        }
        case 'MG': {
            return Tier.MG;
        }
        case 'G': {
            return Tier.G;
        }
        case 'T': {
            return Tier.T;
        }
        case 'C': {
            return Tier.C;
        }
        default: {
            return Tier.KO;
        }
    }
}