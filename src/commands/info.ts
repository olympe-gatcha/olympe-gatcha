import { Command, CommandData } from "../Command";
import { CommandList } from "../CommandList";
import { Character } from "../entity/Character";
import { User } from "../entity/User";
import { has_selected_team, has_user } from "../Middleware";

const cmd = new Command('info');
cmd.description = 'Give info on a character';
cmd.middleWare = [has_user];
cmd.option = [
    {
        name: 'character_id',
        description: 'The id of the character',
        type: 'INTEGER',
        required: true
    }
]

cmd.action = async (d:CommandData) => {
    await charInfo(d.user, d.inter.options.getInteger('character_id'), d.reply);
}

export async function charInfo(user: User, char_id:number, reply: string[]) {
    const char = await Character.findOne({id:char_id},{relations:['team']});
    if (!char) {
        throw 'Cannot find this Character';
    }
    reply.push(char.detailString());
}

CommandList.set(cmd.name, cmd);