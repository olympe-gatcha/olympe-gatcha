import { MessageActionRow, MessageButton, Message } from "discord.js";
import { Command, CommandData } from "../Command";
import { CommandList } from './../CommandList';

const cmd = new Command('channel_id');
cmd.description = 'get the channel id (only useful for dev)';
cmd.middleWare = []
cmd.action = async (d: CommandData) => {
    d.inter.reply({content: `${d.inter.channelId}`, ephemeral : true});
}

CommandList.set(cmd.name, cmd);