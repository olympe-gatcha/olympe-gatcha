import { User } from '../entity/User'
import { Banner } from "../Banner";
import { BannerList } from "../BannerList";
import { Command, CommandData } from "../Command";
import { CommandList } from "../CommandList";
import { Invoke } from "../entity/Invoke";
import { InvokeTable } from "../InvokeTable";
import { has_user } from "../Middleware";
import { TierString } from "../Tier";
import { Tablet } from './../entity/Tablet';
import { TCollectable } from './../TCollectable';

const CHAOS_PULL_DAY = 1;

const cmd = new Command('pull');
cmd.description = 'Try your luck and pull away !'
cmd.middleWare = [has_user];
cmd.option = [
    {
        name: 'banner',
        description: 'The banner to pull from',
        type: 'INTEGER',
        required: false,
        choices:[],
    }
]

// Add each banner to the choice pull
BannerList.forEach((v, k) => {
    cmd.option[0].choices.push({name:v.name, value:k});
});

export async function forcePull(user: User, banner: Banner, result :string[]) : Promise<Invoke> {
    const collect = banner.pull();
    const invo = await collect.collect.addToUser(user, collect.quantity);
    result.push(`You pulled "${collect.collect.string()}"${collect.quantity > 1 ? '(' + collect.quantity + ')':''}`);
    return <Invoke>invo[0];
}

cmd.action = async (d:CommandData) => {
    const banner = d.inter.options.getInteger('banner', false) | 0;
    
    const tablet = await Tablet.findOne({user:d.user, banner_id:banner});
    if (!(tablet || (banner == 0 && d.user.numberOfChaosToday < CHAOS_PULL_DAY))) {
        throw "No more pull for you !";
    }

    await forcePull(d.user, BannerList.get(banner), d.reply);

    // Remove consumable
    if (banner == 0 && d.user.numberOfChaosToday < CHAOS_PULL_DAY) {
        d.user.numberOfChaosToday += 1;
        await d.user.save();
    } else {
        await tablet.consume();
    }
}

CommandList.set(cmd.name, cmd);