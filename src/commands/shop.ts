import { getConnection } from 'typeorm';
import { CollectableType, Shop } from '../entity/Shop';
import { User } from '../entity/User';
import { Command, CommandData } from './../Command';
import { has_user } from './../Middleware';
import { BannerList } from './../BannerList';
import { InvokeTable } from '../InvokeTable';
import { CurrencyList } from '../TCurrency';
import { CommandList } from './../CommandList';
import { elemFromTypeId } from './../entity/Shop';
import { TInvoke } from './../TInvoke';

const cmd = new Command('shop');
cmd.description = 'Shop interaction';
cmd.middleWare = [has_user, update_shop];

CommandList.set(cmd.name, cmd);

//////

const list = new Command('list');
cmd.subcommand.push(list);
list.description = 'List what is in the shop';
list.action = async (d: CommandData) => {
    return printShop(d.user, d.reply);
}

export async function printShop(user: User, reply: string[]) {
    reply.push(`Poggers : ${user.poggers}`);
    reply.push('Shop : ')
    const shop = await Shop.find({user: user});
    if (shop.length == 0) {
        reply.push('    Your Shop is empty');
    } else {
        reply.push(...shop.map(v=>'    ' + v.string()));
    }
}

//////

const buy = new Command('buy');
cmd.subcommand.push(buy);
buy.description = 'Buy from your shop';
buy.option = [
    {
        name: 'shop_id',
        description: 'What you want to buy',
        type: 'INTEGER',
        required: true,
    }
]

buy.action = async (d: CommandData) => {
    const shop_id = d.inter.options.getInteger('shop_id');
    const shop = await Shop.findOne({user:d.user, id:shop_id});

    if (!shop) {
        throw "You can't buy this";
    }
    if (shop.price > d.user.poggers) {
        throw 'You are too poor';
    }

    const elem = elemFromTypeId(shop.type, shop.target_id);
    d.reply.push(`You bought ${(shop.quantity == 1 ? '' : shop.quantity + ' ')}${elem.string()}`);
    d.user.poggers -= shop.price;
    await d.user.save();
    await elem.addToUser(d.user);
    await shop.remove();

}

//////


async function update_shop(d:CommandData) : Promise<CommandData> {
    if (d.user.reset_shop) {
        await rerollShop(d.user);
        d.user.reset_shop = false;
        await d.user.save();
    }
    return d;
}

async function rerollShop(user: User) {
    await getConnection()
    .createQueryBuilder()
    .delete()
    .from(Shop)
    .where('shop."userId" = :id', {id: user.id})
    .execute();

    await addCurrencyToShop(user);
    await addInvokeToShop(user);
    await addTabletToShop(user);
}

async function addCurrencyToShop(user: User) {
    const array = Array.from(CurrencyList);
    let currency = Math.floor(Math.random() * (array.length - 1)) + 1;
    const shop = new Shop();
    shop.user = user;
    shop.type = CollectableType.Currency;
    shop.target_id = currency;
    shop.quantity = Math.floor(Math.random() * 4) + 1;
    shop.price = shop.quantity * CurrencyList.get(currency).price;
    return shop.save();
}

async function addInvokeToShop(user: User) {
    const pull = BannerList.get(0).pull();
    const invoke = <TInvoke>pull.collect;
    const shop = new Shop();
    shop.user = user;
    shop.type = CollectableType.Invoke;
    shop.target_id = invoke.id;
    shop.price = 50 * (invoke.tier + 1)
    return shop.save();
}

async function addTabletToShop(user: User) {
    const array = Array.from(BannerList);
    let banner_id = Math.floor(Math.random() * array.length);
    while (array[banner_id][1].price == 0) {
        banner_id = Math.floor(Math.random() * array.length);
    }
    const shop = new Shop();
    shop.user = user;
    shop.type = CollectableType.Tablet;
    shop.target_id = banner_id;
    shop.price = array[banner_id][1].price;
    return shop.save();
}