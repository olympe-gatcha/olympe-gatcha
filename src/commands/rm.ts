import { Command, CommandData } from "../Command";
import { CommandList } from "../CommandList";
import { Character } from "../entity/Character";
import { User } from "../entity/User";
import { has_selected_team, has_user } from "../Middleware";

const cmd = new Command('rm');
cmd.description = 'Remove a character from the selected team';
cmd.middleWare = [has_user, has_selected_team];
cmd.option = [
    {
        name: 'character_id',
        description: 'The id of the character to remove',
        type: 'INTEGER',
        required: true
    }
]

export async function rmCharFromTeam(user: User, character_id: number, reply: string[]) {
    const char = await Character.findOne({team:user.selectedTeam, id:character_id});
    if (!char) {
        throw 'This char is not part of this team (you can view your char with /list)';
    }
    reply.push(`${char.string()} removed from team ${user.selectedTeam.name}`);
    await char.remove();
}

cmd.action = async (d:CommandData) => {
    await rmCharFromTeam(d.user, d.inter.options.getInteger('character_id'), d.reply);
}

CommandList.set(cmd.name, cmd);