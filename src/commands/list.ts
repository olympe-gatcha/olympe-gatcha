import { Command, CommandData } from "../Command";
import { CommandList } from "../CommandList";
import { has_selected_team, has_user } from "../Middleware";

const cmd = new Command('list');
cmd.description = 'List the character in your selected team'
cmd.middleWare = [has_user, has_selected_team]
cmd.action = async(d:CommandData) => {
    if (d.user.selectedTeam.characters.length == 0) {
        throw `Team ${d.user.selectedTeam.name} is empty (you can add with /add invokeId)`;
    }
    d.reply.push(`${d.user.selectedTeam.name} :`);
    d.reply.push('```md');
    d.reply.push(...d.user.selectedTeam.characters.map(v=>v.string()));
    d.reply.push('```');
}

CommandList.set(cmd.name, cmd);
