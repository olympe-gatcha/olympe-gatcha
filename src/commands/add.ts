import { Command, CommandData } from "../Command";
import { CommandList } from "../CommandList";
import { Character } from "../entity/Character";
import { Invoke } from "../entity/Invoke";
import { User } from "../entity/User";
import { invokeString, InvokeTable } from "../InvokeTable";
import { has_selected_team, has_user } from "../Middleware";

export const TEAM_SIZE = 3;

const cmd = new Command('add');
cmd.description = 'Add Character to team';
cmd.middleWare = [has_user, has_selected_team];
cmd.option = [
    {
        name: 'invoke_id',
        description: 'Invoke id to add to the team',
        type: 'INTEGER',
        required: true,
    }
]

export async function addCharToTeam(user: User, invokeId: number, result: string[] = []) {
    if (!user.selectedTeam.characters) {
        user.selectedTeam.characters = [];
    }
    if (user.selectedTeam.characters.length == TEAM_SIZE) {
        throw `Team ${user.selectedTeam.name} is full (you can remove with with /rm)`;
    }
    const idCount = user.selectedTeam.characters.reduce((cur, char) : number => char.invokeId == invokeId ? cur + 1 : cur, 0);

    if (idCount + 1 > (((await Invoke.findOne({user:user, invokeId:invokeId}))?.count) ?? 0)) {
        throw `You don't have anymore ${invokeString(invokeId)}`;
    }
    const char = new Character();
    char.invokeId = invokeId;
    char.stats = InvokeTable.get(invokeId).stats;
    char.team = user.selectedTeam;
    await char.save();
    user.selectedTeam.characters.push(char);
    await user.selectedTeam.save();
    result.push(`${invokeString(invokeId)} has been added to team ${user.selectedTeam.name}`);
}

cmd.action = async (d:CommandData) => {
    await addCharToTeam(d.user, d.inter.options.getInteger('invoke_id'), d.reply);
}

CommandList.set(cmd.name, cmd);