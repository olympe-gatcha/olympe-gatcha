import { CommandInteraction, Interaction } from "discord.js";
import { Command, CommandData } from "../Command"
import { CommandList } from "../CommandList";
import { has_user } from "../Middleware";


const cmd = new Command('clean');
cmd.description = 'Delete you gatcha acount (ireversible)';

cmd.middleWare = [has_user];

cmd.action = async (d:CommandData) => {
    await d.user.remove();
    d.reply.push('Your account has been deleted');
}

CommandList.set(cmd.name, cmd);