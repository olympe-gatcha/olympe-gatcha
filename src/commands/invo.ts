import { Command, CommandData } from "../Command";
import { CommandList } from "../CommandList";
import { Invoke } from "../entity/Invoke";
import { has_user } from "../Middleware";

const cmd = new Command('invo');
cmd.description = 'List your invocations';
cmd.middleWare = [has_user];
cmd.action = async (d:CommandData) => {
    d.reply.push((await Invoke.find({user:d.user})).map(invo => invo.string()).join('\n'));
}

CommandList.set(cmd.name, cmd);