import { Board } from "../Board";
import { Command, CommandData, replyOnThread } from "../Command";
import { CommandList } from "../CommandList";
import { TeamInstance } from "../entity/TeamInstance";
import { User } from "../entity/User";
import { has_selected_team, has_user } from "../Middleware";

const cmd = new Command('fight');
cmd.description = 'Fight an other player for fame and glory';
cmd.middleWare = [has_user, has_selected_team];
cmd.option = [
    {
        name: 'target',
        description: 'User to fight',
        type: 'USER',
        required: false,
    }
]

cmd.action = async (d:CommandData)=> {
    const target_discord_user_id = d.inter.options.getUser('target', false)?.id || 'server';
    const target_user = await User.findOne({discordId:target_discord_user_id}, {relations:['selectedTeam']});

    if (!target_user) {
        throw `${target_discord_user_id} has no gatcha account`;
    }
    fight(d.user, target_user, d.reply);
    await replyOnThread(d, `Fight between ${d.user.name} and ${target_user.name}\n${d.reply[d.reply.length - 1]}`, {name: `Fight between ${d.user.name} and ${target_user.name}`, autoArchiveDuration: 60});
}

CommandList.set(cmd.name, cmd);

export function fight(user: User, target_user: User, reply:string[]) {
    if (!target_user.selectedTeam) {
        throw `${target_user.name} has no selected team`
    }
    if (user == target_user) {
        throw `Why do you wanna hit yourself ?`
    }
    const teamA = new TeamInstance();
    teamA.from(user.selectedTeam);

    const teamB = new TeamInstance();
    teamB.from(target_user.selectedTeam);

    combat(teamA, teamB, reply);
}

function combat(team1: TeamInstance, team2: TeamInstance, reply:string[]) {
    const board = new Board(team1, team2);
    const ret = board.fight(reply);
    reply.push(`Winner : ${ret.team.user.name}`)
}