import { BannerList } from "../BannerList";
import { Command, CommandData } from "../Command";
import { CommandList } from "../CommandList";
import { User } from "../entity/User";
import { addCharToTeam } from "./add";
import { forcePull } from "./pull";
import { createTeam } from "./team";
import { Invoke } from './../entity/Invoke';
import { TInvoke } from './../TInvoke';

const cmd = new Command('gatcha');
cmd.description = 'Begin your journey into the gatcha World !';
cmd.action = async (d:CommandData)=>{
    await gatcha(d.inter.member.user.id, d.inter.user.username, d.reply);
}

export async function gatcha(discordId: string, name: string, reply: string[]) {
    var user = await User.findOne({discordId: discordId});
    
    if (user) {
        throw 'You already have an account';
    }
    user = new User();
    user.discordId = discordId;
    user.name = name;
    user = await user.save();
    
    reply.push(`Welcome ${user.name}\nYou have 1 Chaos Pull and 3 Pantheon run every day, don't forget to use them!`);
    
    const inva = await forcePull(user, BannerList.get(0), reply);
    const invb = await forcePull(user, BannerList.get(0), reply);
    const invc = await forcePull(user, BannerList.get(0), reply);
    await createTeam(user, 'TeamA', reply);

    await addCharToTeam(user, inva.invokeId, reply);
    await addCharToTeam(user, invb.invokeId, reply);
    await addCharToTeam(user, invc.invokeId, reply);
}

cmd.middleWare = []

CommandList.set(cmd.name, cmd);