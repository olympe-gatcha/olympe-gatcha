
import { MessageActionRow, MessageButton, MessageSelectMenu } from 'discord.js';
import { Command, CommandData } from './../Command';
import { Message } from 'discord.js';
import { CommandList } from './../CommandList';
import { Equipable } from './../entity/Equipable';
import { has_user } from './../Middleware';
import { User } from './../entity/User';
import { EquipablesTable } from './../EquipablesTable';
import { LinesTable } from '../TLine';

const cmd = new Command('craft');
cmd.description = 'Craft your items';
cmd.middleWare = [has_user]
cmd.action = async (d: CommandData) => {
    let selected = undefined;
    const msg = <Message>await d.inter.reply({content: 'Select an item bellow', components: [await createSelectRow(d.user)], fetchReply: true});
    const collector = msg.createMessageComponentCollector({idle: 60000});
    collector.on('collect', async i => {
        if (i.user.id === d.inter.user.id) {
            if (i.isSelectMenu()) {
                selected = await Equipable.findOne(i.values[0]);
                await i.update({content: itemDetail(selected), components: await createComponent(d.user, selected)});
            } else if (i.isButton()) {
                switch (i.customId) {
                    case 'alteration':
                        selected.alteration();
                        d.user.alters -= 1;
                        await d.user.save();
                        await selected.save();
                        break;
                    case 'divination':
                        selected.divination();
                        d.user.divs -= 1;
                        await d.user.save();
                        await selected.save();
                        break;
                    case 'favor_suff':
                        selected.favor_suffix();
                        d.user.favors -= 1;
                        await d.user.save();
                        await selected.save();
                        break;
                    case 'favor_pref': 
                        selected.favor_prefix();
                        d.user.favors -= 1;
                        await d.user.save();
                        await selected.save();
                        break;
                    default:
                        throw 'WTF ' + i.customId;
                }
                await i.update({content: itemDetail(selected), components: await createComponent(d.user, selected)});
            }
        } else {
            i.reply({content: 'Not your message pal', ephemeral: true});
        }
    })

    collector.on('end', async collected => {
        await msg.delete();
    })
}

async function createComponent(user: User, selected?: Equipable) {
    let ret : MessageActionRow[] = [await createSelectRow(user, selected)];

    if (selected) {
        ret.push(createButtonRow(user, selected));
    }
    return ret;
}


async function createSelectRow(user: User, selected?: Equipable) : Promise<MessageActionRow>{
    const items = await Equipable.find({user: user});

    if (items.length == 0) {
        throw "You don't have any item (you poor)"
    }

    const ret = new MessageActionRow()
    .addComponents(
        new MessageSelectMenu().setCustomId('item').setPlaceholder('Select an item to craft')
        .addOptions(items.map((v:Equipable)=> {
            return {label: v.name(), value: '' + v.id, default: selected?.id == v.id ? true : false}
        }))
    )

    return ret;
}

function itemDetail(selected: Equipable) : string {
    const item = EquipablesTable.get(selected.eqId);
    let ret : string = selected.name();
    ret += '\n```md\n'
    ret += 'Prefix : \n';
    for (let tier = item.tierMin; tier <= selected.tier; tier += 1) {
        const array = item.rollable_prefixes.get(tier);
        if (!array) {
            continue;
        }
        for (let i = 0; i < array.length; i++) {
            const tmp = array[i];
            const line = LinesTable.get(tmp[0]);

            if (selected.prefix.idLine === tmp[0]) {
                ret += '# ';
                ret += selected.prefix.string();
                ret += ` (${tmp[1]})`;
                ret += '\n';
            } else {
                ret += line.string();
                ret += ` (${tmp[1]})`;
                ret += '\n';
            }
        }
    }

    ret += '\nSuffix : \n';
    for (let tier = item.tierMin; tier <= selected.tier; tier += 1) {
        const array = item.rollable_sufixes.get(tier);
        if (!array) {
            continue;
        }
        for (let i = 0; i < array.length; i++) {
            const tmp = array[i];
            const line = LinesTable.get(tmp[0]);

            if (selected.suffix.idLine === tmp[0]) {
                ret += '# ';
                ret += selected.suffix.string();
                ret += ` (${tmp[1]})`;
                ret += '\n';
            } else {
                ret += line.string();
                ret += ` (${tmp[1]})`;
                ret += '\n';
            }
        }
    }
    ret += '```'
    return ret;
}

function createButtonRow(user: User, selected?: Equipable) {
    const ret = new MessageActionRow()
    if (!selected) {
        return ret;
    }
    ret.addComponents(
        new MessageButton()
        .setCustomId('alteration')
        .setLabel(`Alteration(${user.alters})`)
        .setDisabled(user.alters > 0 ? false : true)
        .setStyle('PRIMARY')
    )

    ret.addComponents(
        new MessageButton()
        .setCustomId('divination')
        .setLabel(`Divination(${user.divs})`)
        .setDisabled(user.divs > 0 ? false : true)
        .setStyle('PRIMARY')
    )
    
    ret.addComponents(
        new MessageButton()
        .setCustomId('favor_pref')
        .setLabel(`Favor Prefix(${user.favors})`)
        .setDisabled(user.favors > 0 ? false : true)
        .setStyle('PRIMARY')
    )

    ret.addComponents(
        new MessageButton()
        .setCustomId('favor_suff')
        .setLabel(`Favor Suffix(${user.favors})`)
        .setDisabled(user.favors > 0 ? false : true)
        .setStyle('PRIMARY')
    )

    // 'alteration', 'divination', 'favor_suff', 'favor_pref'
    return ret;
}

CommandList.set(cmd.name, cmd);