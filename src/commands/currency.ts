
import { Command, CommandData } from './../Command';
import { has_user } from './../Middleware';
import { Tablet } from './../entity/Tablet';
import { CommandList } from './../CommandList';

const cmd = new Command('currency');
cmd.description = 'List your currency and tablet'
cmd.middleWare = [has_user]
cmd.action = async (d:CommandData) => {
    d.reply.push(`Poggers: ${d.user.poggers}`);
    d.reply.push(`Favors of Hephaestus: ${d.user.favors}`);
    d.reply.push(`Divination Orb: ${d.user.divs}`);
    d.reply.push(`Alteration Orb: ${d.user.alters}`);
    d.reply.push('Tablets :');
    const tables = await Tablet.find({user:d.user});
    if (tables.length == 0) {
        d.reply.push('    No tablet');
    } else {
        tables.forEach(v=>d.reply.push(`    ${v.string()}`));
    }
}

CommandList.set(cmd.name, cmd);