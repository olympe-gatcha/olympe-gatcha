import { Command, CommandData } from "../Command";
import { CommandList } from "../CommandList";
import { Team } from "../entity/Team";
import { User } from "../entity/User";
import { has_user } from "../Middleware";


const cmd = new Command('team');
cmd.description = 'Edit Team';
cmd.middleWare = [has_user];

CommandList.set(cmd.name, cmd);

//////

const list = new Command('list');
cmd.subcommand.push(list);
list.description = 'List your teams';
list.action = async (d: CommandData) => {
    const teams = await Team.find({user: d.user});
    if (teams.length == 0) {
        throw "You d'ont have any team yet (you can create on with /team create teamName)";
    }
    d.reply.push(teams.map(v=>v.string()).join('\n'));
}

//////

const create = new Command('create');
cmd.subcommand.push(create);
create.description = 'Create a new team';
create.option.push({
    name: 'team_name',
    description: 'The name of the new team',
    type: 'STRING',
    required: true
});

export async function createTeam(user: User, team_name: string, reply: string[]) {
    if (await Team.findOne({user:user, name: team_name})) {
        throw 'This team already exists';
    }
    const team = new Team();
    team.user = user;
    team.name = team_name;
    await team.save();
    user.selectedTeam = team;
    await user.save();
    reply.push(`Team "${team.name}" has been created and Selected`);
}

create.action = async (d:CommandData) => {
    const team_name = d.inter.options.getString('team_name');
    await createTeam(d.user, team_name, d.reply);
}

//////

const select = new Command('select');
cmd.subcommand.push(select);
select.description = 'Select the team';
select.option = [
    {
        name: 'team_name',
        description: 'The team to select',
        type: 'STRING',
        required : true,
    }
]
select.action = async (d:CommandData) => {
    const team = await Team.findOne({user:d.user, name: d.inter.options.getString('team_name')});
    if (!team) {
        throw `You don't have a team ${d.inter.options.getString('team_name')} (you can list your teams with /team list)`;
    }
    d.user.selectedTeam = team;
    await d.user.save();
    d.reply.push(`Team '${team.name}' selected`);
}

//////


const rm = new Command('rm');
cmd.subcommand.push(rm);
rm.description = 'Delete a team'
rm.option = [
    {
        name: 'team_name',
        description: 'The team to delete',
        type: 'STRING',
        required: false
    }
]
rm.action = async (d: CommandData) => {
    const team_name = d.inter.options.getString('team_name', false);
    var team : Team;
    if (team_name) {
        team = await Team.findOne({user:d.user, name: d.inter.options.getString('team_name')});
        if (!team) {
            throw `You don't have a team ${d.inter.options.getString('team_name')} (you can list your teams with /team list)`;
        }
    } else {
        if (!d.user.selectedTeam) {
            throw 'You must either select a team or pass a team name';
        }
        team = d.user.selectedTeam;
    }
    await team.remove();
    d.reply.push(`Team ${team.name} has been removed`);
}