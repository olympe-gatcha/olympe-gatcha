import { Command, CommandData } from './../Command';
import { has_user } from './../Middleware';
import { CommandList } from './../CommandList';
import { Equipable } from './../entity/Equipable';

const cmd = new Command('item');
cmd.description = 'Manage your items';
cmd.middleWare = [has_user];

CommandList.set(cmd.name, cmd);

/////

const list = new Command('list');
cmd.subcommand.push(list);
list.description = 'List your items';
list.action = async (d: CommandData) => {
    const items =  await Equipable.find({user: d.user});
    if (items.length == 0) {
        throw "You don't have any items (you poor)"
    }
    d.reply.push(...items.map(v=>v.name()));
}
