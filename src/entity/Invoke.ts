import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToMany } from "typeorm";
import { invokeString } from "../InvokeTable";
import { User } from './User';

@Entity()
export class Invoke extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, user => user.characters, {nullable: false, onDelete: 'CASCADE'})
    user: User;

    @Column("int")
    invokeId: number;

    @Column("int")
    count: number;

    string() : string {
        return `${invokeString(this.invokeId)} : ${this.count}`;
    }
}