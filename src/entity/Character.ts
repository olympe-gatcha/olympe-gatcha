import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany} from "typeorm";
import { Equipable } from './Equipable';
import { invokeString } from "../InvokeTable";
import { Team } from "./Team";
import { Stats, statString } from "../Stats";

@Entity()
export class Character extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    invokeId: number;

    @ManyToMany(() => Equipable, equipable => equipable.characters)
    equipables: Equipable[];

    @Column('json')
    stats: Stats;

    @ManyToOne(()=>Team, team => team.characters, { nullable:false, onDelete:'CASCADE'})
    team: Team;

    string() : string {
        return invokeString(this.invokeId) + `(${this.id})`;
    }

    detailString() : string {
        return `user: ${this.team.user.name}\nteam: ${this.team.name}\n${this.string()} :${statString(this.stats)}`;
    }
}