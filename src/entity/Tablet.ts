import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from './User';
import { BannerList } from './../BannerList';

@Entity()
export class Tablet extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, {nullable: false, onDelete: 'CASCADE'})
    user: User;

    @Column('int')
    banner_id: number;

    @Column('int')
    count: number;

    string() {
        return `${BannerList.get(this.banner_id).name}(${this.banner_id}): ${this.count}`
    }

    async consume() {
        this.count -= 1;
        if (this.count <= 0) {
            return this.remove();
        }
        return this.save();
    }
}