import { Entity, BaseEntity, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { Character } from "./Character";
import { CharInstance } from './CharInstance';
import { Team } from "./Team";

@Entity()
export class TeamInstance extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(()=> Team, {eager: true})
    @JoinColumn()
    team: Team;

    @OneToMany(()=>CharInstance, charInstance => charInstance.team, {eager: true})
    characters: CharInstance[];

    from(team : Team) {
        this.team = team;
        this.characters = team.characters.map((char : Character) : CharInstance => {
            const ret = new CharInstance();
            ret.from(char);
            ret.team = this;
            return ret;
        });
    }
}