import { Entity, BaseEntity, PrimaryGeneratedColumn, ManyToOne, Column } from "typeorm";
import { Character } from './Character';
import { TeamInstance } from './TeamInstance';
import { Stats } from './../Stats';

@Entity()
export class CharInstance extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Character, {nullable: false, eager: true, onDelete: 'CASCADE'})
    character: Character;

    @ManyToOne(()=>TeamInstance, teamInstance => teamInstance.characters)
    team: TeamInstance;

    @Column('json')
    stats: Stats

    string() : string {
        return `${this.team.team.user.name} ${this.character.string()}`
    }

    from(char : Character) {
        this.character = char;
        this.stats = char.stats;
    }
}