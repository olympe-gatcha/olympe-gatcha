import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";
import { TInvoke } from './../TInvoke';
import { TTablet } from './../TTablet';
import { CurrencyList, TCurrency } from "../TCurrency";
import { InvokeTable } from "../InvokeTable";
import { EquipablesTable } from './../EquipablesTable';
import { TEquipable } from './../TEquipable';

export enum CollectableType {
    Currency,
    Invoke,
    Tablet,
    Equipable,
}

@Entity()
export class Shop extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, {nullable: false, onDelete: 'CASCADE'})
    user: User;

    @Column('int')
    price: number;

    @Column({
        type:'enum',
        enum: CollectableType,
    })
    type: CollectableType;

    @Column('int')
    target_id: number;

    @Column('int', {default: 1})
    quantity: number

    string() {
        return `${this.id}. ${elemFromTypeId(this.type, this.target_id).string()}${(this.quantity > 1? `(${this.quantity})` : '')}: ${this.price}`;
    }
}

export function elemFromTypeId(type: CollectableType, id: number) : TInvoke | TTablet | TCurrency | TEquipable {
    switch (type) {
        case CollectableType.Currency: {
            return CurrencyList.get(id);
        }
        case CollectableType.Invoke: {
            return InvokeTable.get(id);
        }
        case CollectableType.Tablet: {
            return new TTablet(id);
        }
        case CollectableType.Equipable: {
            return EquipablesTable.get(id);
        }
    }
}