import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { Invoke } from './Invoke';
import { Equipable } from './Equipable';
import { Team } from './Team';

@Entity()
export class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    discordId: string;

    @Column()
    name: string;

    @Column("int", {default: 0})
    numberOfChaosToday: number;

    @Column("int", {default: 0})
    numberOfPantheonToday: number;

    @OneToMany(()=>Invoke, invoke => invoke.user)
    characters: Invoke[];

    @OneToMany(()=>Equipable, equipable => equipable.user)
    equipables: Equipable[];

    @OneToMany(()=>Team, team => team.user)
    teams: Team[];

    @OneToOne(()=>Team, {onDelete:'SET NULL'})
    @JoinColumn()
    selectedTeam: Team;

    @Column('int', {default: 100})
    poggers: number;

    @Column('int', {default: 0})
    favors: number;

    @Column('int', {default: 0})
    divs: number;

    @Column('int', {default: 0})
    alters: number;

    @Column({default: true})
    reset_shop: boolean;
}
