import { Entity, BaseEntity, PrimaryGeneratedColumn, ManyToOne, Column, JoinTable, OneToMany } from "typeorm";
import { User } from './User';
import { Character } from './Character';

@Entity()
export class Team extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(() => User, user => user.characters, {nullable: false, onDelete:'CASCADE', eager: true})
    user: User;

    @OneToMany(()=>Character, character => character.team, {eager: true})
    characters: Character[];

    string() : string{
        return `${this.name} (${this.characters.map(v=>v.string()).join(', ')})`
    }
}