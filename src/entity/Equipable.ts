import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, AfterLoad } from "typeorm";
import { User } from './User';
import { Character } from './Character';
import { Line } from "../TLine";
import { Tier } from "../Tier";
import { EquipablesTable } from './../EquipablesTable';
import { LinesTable } from './../TLine';

@Entity()
export class Equipable extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, user => user.equipables, {nullable: false, onDelete: 'CASCADE'})
    user: User;

    @Column("int")
    eqId: number;

    @Column({
        type:'enum',
        enum: Tier,
    })
    tier: Tier;

    @ManyToMany(()=>Character, character => character.equipables)
    @JoinTable()
    characters: Character[];

    @Column('json')
    implicit: Line;

    @Column('json')
    prefix: Line;

    @Column('json')
    suffix: Line;

    name() : string {
        return `${this.prefix.idLine != 0 ? this.prefix.name() + ' ' : ''}${EquipablesTable.get(this.eqId).name}[${this.eqId}]${this.suffix.idLine != 0 ? ' of ' + this.suffix.name(): ''} (${this.id})`;
    }

    @AfterLoad()
    makeRealLine() {
        this.implicit = new Line(this.implicit.idLine, this.implicit.data);
        this.prefix = new Line(this.prefix.idLine, this.prefix.data);
        this.suffix = new Line(this.suffix.idLine, this.suffix.data);
    }

    alteration() {
        this.prefix = EquipablesTable.get(this.eqId).rollPrefix(this.tier);
        this.suffix = EquipablesTable.get(this.eqId).rollSuffix(this.tier);
    }

    divination() {
        this.prefix = LinesTable.get(this.prefix.idLine).rollLine();
        this.suffix = LinesTable.get(this.suffix.idLine).rollLine();
    }

    favor_prefix() {
        this.prefix = EquipablesTable.get(this.eqId).rollPrefix(this.tier);
    }

    favor_suffix() {
        this.suffix = EquipablesTable.get(this.eqId).rollSuffix(this.tier);
    }
}