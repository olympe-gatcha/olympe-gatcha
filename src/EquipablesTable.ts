import { Board } from "./Board";
import { Color } from "./Color";
import { CharInstance } from "./entity/CharInstance";
import { TEquipable } from "./TEquipable";
import { Tier } from "./Tier";
import { LinesTable } from './TLine';

export const EquipablesTable = new Map<number, TEquipable>();

function has_crit(source: CharInstance) : boolean {
    return ((Math.random() > source.stats.critChance) ? false : true);
}

new TEquipable(0,
    'Auto Attack',
    Color.White,
    Tier.KO,
    LinesTable.get(0).rollLine(),
    new Map<Tier, [number, number][]>(),
    new Map<Tier, [number, number][]>(),
    function (b: Board, source: CharInstance, targets: CharInstance[], reply: string[]) {
        const target = targets[0];
        if (!target) {
            console.error('No target !');
            return;
        }
        const crit = has_crit(source);
        var damage = source.stats.atk;
        if (crit) {
            damage *= 2; // Char damage * Critmulti if crit
        }
        damage -= target.stats.def; // remove target def from damage

        if (damage < 0) { // So that an attack doesn't heal the target ...
            damage = 0;
        }
        target.stats.hp -= damage;
        reply.push(`<${source.string()}> uses <AutoAttack> dealing ${crit ? '[ ](CRITICAL) ' : ''}[${damage}] damage to <${target.string()}> [ ](${target.stats.hp} hp left)`)

        if (target.stats.hp <= 0) { // If target Dead
            b.deathOfChar(target, reply);
        }
    },
    function (b: Board, source: CharInstance) {
        for (const c of b.chars) {
            if (c.team.team.id != source.team.team.id && c.stats.hp > 0) {
                return [c];
            }
        }
        return [];
    });

new TEquipable(1,
    'Epée',
    Color.Red,
    Tier.K,
    LinesTable.get(1).rollLine(50),
    new Map<Tier, [number, number][]>([
        [Tier.K, [
            [0,100], // Nothing
            [1, 50], // Attack+
        ]],
        [Tier.N, [
            [1, 50], // Attack+
        ]]
    ]),
    new Map<Tier, [number, number][]>([
        [
            Tier.K, [
                [0, 100], // Nothing
                [2, 50], // Def+
            ]
        ],
    ]));
