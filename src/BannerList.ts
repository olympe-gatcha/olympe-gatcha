import { Banner } from "./Banner";
import { CollectableType } from "./entity/Shop";
import { InvokeTable } from "./InvokeTable";
import { Tier } from "./Tier";
import { TInvoke } from "./TInvoke";
import { EquipablesTable } from './EquipablesTable';
import { TEquipable } from './TEquipable';



export const BannerList = new Map<number, Banner>();

export function generateDefaultBaner() {
    console.log('generate Chaos');
    let b = new Banner('Chaos');
    b.price = 100;
    InvokeTable.forEach((value: TInvoke) => {
        if (value.tier != Tier.KO) {
            b.add({id: value.id, type: CollectableType.Invoke, weight: 10 * (7 - value.tier)});
        }
    });
    BannerList.set(0, b);

    console.log('generate Gilgamesh Tresury');
    b = new Banner('Gilgamesh Tresury');
    b.price = 100;
    EquipablesTable.forEach((value: TEquipable) => {
        if (value.tierMin != Tier.KO) {
            b.add({id: value.id, type: CollectableType.Equipable, weight: 10 * (7 - value.tierMin)})
        }
    });

    BannerList.set(1, b);
}


