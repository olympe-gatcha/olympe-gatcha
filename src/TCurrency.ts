import { User } from './entity/User';
import { TCollectable } from './TCollectable';

export class TCurrency extends TCollectable {
    price: number; // price of one in poggers
}

export const CurrencyList : Map<number, TCurrency> = new Map();

CurrencyList.set(0, {
    string: function() {return 'Pogger'},
    price: 1,
    addToUser: async function(u: User, quantity: number = 1) {
        u.poggers += quantity;
        await u.save();
        return [];
    }
});

CurrencyList.set(1, {
    string: function() {return 'Favor of Hephaestus'},
    price: 200,
    addToUser: async function(u: User, quantity: number = 1) {
        u.favors += quantity;
        await u.save();
        return [];
    }
});

CurrencyList.set(2, {
    string: function() {return 'Divination Orb'},
    price: 150,
    addToUser: async function(u: User, quantity: number = 1) {
        u.divs += quantity;
        await u.save();
        return [];
    }
});

CurrencyList.set(3, {
    string: function() {return 'Alteration Orb'},
    price: 50,
    addToUser: async function(u: User, quantity: number = 1) {
        u.alters += quantity;
        await u.save();
        return [];
    }
});