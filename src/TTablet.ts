import { TCollectable } from './TCollectable';
import { BannerList } from './BannerList';
import { User } from './entity/User';
import { Tablet } from './entity/Tablet';

export class TTablet extends TCollectable {
    banner_id: number;

    constructor(banner_id: number) {
        super();
        if (!BannerList.get(banner_id)) {
            throw `Banner ${banner_id} does not exists`;
        }
        this.banner_id = banner_id;
    }

    string() {
        return `${BannerList.get(this.banner_id).name} (${this.banner_id})`;
    }

    async addToUser(u: User, quantity: number = 1) {
        let tablet = await Tablet.findOne({user: u, banner_id:this.banner_id});
        if (tablet) {
            tablet.count += quantity;
        } else {
            tablet = new Tablet();
            tablet.user = u;
            tablet.banner_id = this.banner_id;
            tablet.count = quantity;
        }

        await tablet.save();
        return  [tablet];
    }
}
